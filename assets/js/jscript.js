// Using Jquery
$(document).ready(function () { //document Open
    
  // menu trigger
  $('#menuToggle').click(function(){
    $('#primary-nav').style.display = 'block';
  });

  // slick slider
  var $status = $('.pagingInfo');
  var $slider = $('.slider')
  var $progressBar = $('.progress')
  var $progressBarLabel = $('.slider__label')

  $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    var calc = (nextSlide / (slick.slideCount - 1)) * 100

    $progressBar
      .css('background-size', calc + '% 100%')
      .attr('aria-valuenow', calc)

    $progressBarLabel.text(calc + '% completed')
  })

  $slider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  })

  // slider change on mouse scroll
  $slider.on('wheel', function (e) {
    e.preventDefault()
    if (e.originalEvent.deltaY > 0) {
      $(this).slick('slickNext')
    } else {
      $(this).slick('slickPrev')
    }
  })

  // button off after three second
  setTimeout(function () {
    $('#mute-button').remove()
  }, 3000)

  // get exact slider number to the slider
  $slider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0);
    console.log('index of slider', i); 
    $status.text(i + '/' + (slick.slideCount-1));
  });
// end of Jquery
});
